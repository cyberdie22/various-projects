BITS 64
    global  _start
    SECTION .text
_start:
    mov     rax, 1
    mov     rdi, 0
    mov     rsi, CHEESE
    mov     rdx, 47
    syscall
    jmp     _start

    mov     rax, 60
    xor     rdi, rdi
    syscall

    SECTION .data
CHEESE: db  "pneumonoultramicroscopicsilicovolcanoconiosis", 0xA, 0