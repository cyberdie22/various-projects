FROM gitpod/workspace-full-vnc:latest

RUN sudo apt-get update && \
    sudo apt-get install -y libgtk-3-dev && \
    sudo rm -rf /var/lib/apt/lists/*

RUN sudo apt-get update && \
    sudo apt-get install -y qemu uml-utilities virt-manager git wget libguestfs-tools p7zip-full make fish && \
    sudo rm -rf /var/lib/apt/lists/*


# Give back control
USER root