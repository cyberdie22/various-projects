import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    kotlin("jvm") version "1.6.0-M1"
    id("com.github.johnrengelman.shadow") version "7.0.0"
}

group = "me.cyberdie22"
version = "2021.0.0"

repositories {
    mavenCentral()
}

dependencies {

}

tasks {
    withType<ShadowJar> {
        minimize()
        manifest {
            attributes(mapOf(
                "Main-Class" to "me.cyberdie22.clipcompiler.MainKt"
            ))
        }
    }
}