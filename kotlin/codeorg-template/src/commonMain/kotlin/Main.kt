val didit = false
fun draw() {
    if (!didit) {
        val string = "Hello, world!"
        string.forEach { it ->
            print(it)
        }
    }
}

fun main() {
    draw()
}