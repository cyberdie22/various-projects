const esbuild = require('esbuild')
const babel = require("@babel/core");

const fs = require('fs')

let settings = {
    bundle: true,
    format: 'iife',
    outfile: 'out.js',
    allowOverwrite: true,
}

esbuild.buildSync({
    ...settings,
    entryPoints: ['build/distributions/codeorg-template.js', 'build/'],
    minify: true,
})

esbuild.buildSync({
    ...settings,
    entryPoints: ['out.js'],
    minify: false,
})

const code = fs.readFileSync('out.js')
const transformedCode = babel.transformSync(code, {
    // no options yet
});
fs.writeFileSync('out.js', transformedCode.code)