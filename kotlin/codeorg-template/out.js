"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function () {
  (function () {
    var q = function q(h, r) {
      return function () {
        return r || h((r = {
          exports: {}
        }).exports, r), r.exports;
      };
    };

    var X = q(function (L) {
      (function (h, r) {
        (typeof exports === "undefined" ? "undefined" : _typeof(exports)) == "object" && (typeof module === "undefined" ? "undefined" : _typeof(module)) == "object" ? module.exports = r() : typeof define == "function" && define.amd ? define([], r) : (typeof exports === "undefined" ? "undefined" : _typeof(exports)) == "object" ? exports["codeorg-template"] = r() : h["codeorg-template"] = r();
      })(L, function () {
        return h = {
          490: function _(N) {
            (function (O) {
              "use strict";

              function $() {}

              function y() {}

              function R() {}

              var T, I;

              function g() {}

              function b(e) {
                g.call(this), this._outputStream = e;
              }

              function f() {
                c.call(this);
              }

              function c() {
                g.call(this), this._buffer = "";
              }

              function U() {
                I = this, this._MIN_VALUE = new n(0), this._MAX_VALUE = new n(65535), this._MIN_HIGH_SURROGATE = new n(55296), this._MAX_HIGH_SURROGATE = new n(56319), this._MIN_LOW_SURROGATE = new n(56320), this._MAX_LOW_SURROGATE = new n(57343), this._MIN_SURROGATE = new n(55296), this._MAX_SURROGATE = new n(57343), this._SIZE_BYTES = 2, this._SIZE_BITS = 16;
              }

              function j() {
                return I == null && new U(), I;
              }

              function n(e) {
                var t;
                j(), t = 65535 & e, this._value = t;
              }

              function B(e, t) {
                var o;

                if (function (v) {
                  return typeof v == "string";
                }(e)) {
                  var a,
                      s,
                      u = e.charCodeAt(t);
                  if (j(), u < new n(0).toInt_0_k$() ? s = true : (j(), s = u > new n(65535).toInt_0_k$()), s) throw G("Invalid Char code: " + u);
                  a = function (v) {
                    var S,
                        A = function (i) {
                      return i instanceof k ? i.toInt_0_k$() : function (p) {
                        return p > 2147483647 ? 2147483647 : p < -2147483648 ? -2147483648 : function (d, E) {
                          return 0 | d;
                        }(p);
                      }(i);
                    }(v);

                    return S = function (i) {
                      var p = function (d) {
                        return d << 16 >> 16;
                      }(i);

                      return p;
                    }(A), new n(S);
                  }(u), o = a;
                } else o = e.get_ha5a7z_k$(t);

                return o;
              }

              function w(e, t) {
                Error.captureStackTrace != null ? Error.captureStackTrace(e, t) : e.stack = new Error().stack;
              }

              function C(e, t) {
                return Object.getPrototypeOf(e).hasOwnProperty(t);
              }

              function k() {}

              function _() {
                w(this, _);
              }

              function H(e, t) {
                return function (o, a) {
                  (function (s, u) {
                    (function (v, S, A) {
                      Error.call(v), function (i, p, d) {
                        if (!C(i, "message")) {
                          var E;

                          if (p == null) {
                            var x;

                            if (p !== null) {
                              var M = d == null ? null : d.toString();
                              x = M !== null && M !== void 0 ? M : void 0;
                            } else x = void 0;

                            E = x;
                          } else E = p;

                          i.message = E;
                        }

                        C(i, "cause") || (i.cause = d), i.name = Object.getPrototypeOf(i).constructor.name;
                      }(v, S, A);
                    })(u, s, void 0), _.call(u);
                  })(o, a), m.call(a);
                }(e, t), l.call(t), t;
              }

              function G(e) {
                var t = H(e, Object.create(l.prototype));
                return w(t, G), t;
              }

              function l() {
                w(this, l);
              }

              function m() {
                w(this, m);
              }

              b.prototype = Object.create(g.prototype), b.prototype.constructor = b, c.prototype = Object.create(g.prototype), c.prototype.constructor = c, f.prototype = Object.create(c.prototype), f.prototype.constructor = f, k.prototype = Object.create(y.prototype), k.prototype.constructor = k, _.prototype = Object.create(Error.prototype), _.prototype.constructor = _, m.prototype = Object.create(_.prototype), m.prototype.constructor = m, l.prototype = Object.create(m.prototype), l.prototype.constructor = l, $.$metadata$ = {
                simpleName: "Comparable",
                kind: "interface",
                interfaces: []
              }, y.$metadata$ = {
                simpleName: "Number",
                kind: "class",
                interfaces: []
              }, R.prototype.toString = function () {
                return "kotlin.Unit";
              }, R.$metadata$ = {
                simpleName: "Unit",
                kind: "object",
                interfaces: []
              }, g.$metadata$ = {
                simpleName: "BaseOutput",
                kind: "class",
                interfaces: []
              }, b.prototype.print_qi8yb4_k$ = function (e) {
                var t = String(e);

                this._outputStream.write(t);
              }, b.$metadata$ = {
                simpleName: "NodeJsOutput",
                kind: "class",
                interfaces: []
              }, f.prototype.print_qi8yb4_k$ = function (e) {
                var t = String(e),
                    o = t.lastIndexOf("\n", 0);

                if (o >= 0) {
                  var a,
                      s = this._get_buffer__0_k$();

                  a = t.substring(0, o), this._set_buffer__a4enbm_k$(s + a), this.flush_sv8swh_k$();
                  var u = o + 1 | 0;
                  t = t.substring(u);
                }

                this._set_buffer__a4enbm_k$(this._get_buffer__0_k$() + t);
              }, f.prototype.flush_sv8swh_k$ = function () {
                console.log(this._get_buffer__0_k$()), this._set_buffer__a4enbm_k$("");
              }, f.$metadata$ = {
                simpleName: "BufferedOutputToConsoleLog",
                kind: "class",
                interfaces: []
              }, c.prototype._set_buffer__a4enbm_k$ = function (e) {
                this._buffer = e;
              }, c.prototype._get_buffer__0_k$ = function () {
                return this._buffer;
              }, c.prototype.print_qi8yb4_k$ = function (e) {
                var t,
                    o = this._buffer;
                t = String(e), this._buffer = o + t;
              }, c.$metadata$ = {
                simpleName: "BufferedOutput",
                kind: "class",
                interfaces: []
              }, U.$metadata$ = {
                simpleName: "Companion",
                kind: "object",
                interfaces: []
              }, n.prototype.toInt_0_k$ = function () {
                return this._value;
              }, n.prototype.toString = function () {
                return String.fromCharCode(this._value);
              }, n.$metadata$ = {
                simpleName: "Char",
                kind: "class",
                interfaces: [$]
              }, k.prototype.toInt_0_k$ = function () {
                return this._low;
              }, k.$metadata$ = {
                simpleName: "Long",
                kind: "class",
                interfaces: [$]
              }, _.$metadata$ = {
                simpleName: "Exception",
                kind: "class",
                interfaces: []
              }, l.$metadata$ = {
                simpleName: "IllegalArgumentException",
                kind: "class",
                interfaces: []
              }, m.$metadata$ = {
                simpleName: "RuntimeException",
                kind: "class",
                interfaces: []
              }, T = typeof process != "undefined" && process.versions && process.versions.node ? new b(process.stdout) : new f(), function () {
                for (var e = "Hello, world!", t = 0, o = e.length; t < o;) {
                  var a = B(e, t);
                  t = t + 1 | 0, s = a, T.print_qi8yb4_k$(s);
                }

                var s;
              }();
            })(N.exports);
          }
        }, r = {}, function N(O) {
          var $ = r[O];
          if ($ !== void 0) return $.exports;
          var y = r[O] = {
            exports: {}
          };
          return h[O](y, y.exports, N), y.exports;
        }(490);
        var h, r;
      });
    });
    X();
  })();
})();