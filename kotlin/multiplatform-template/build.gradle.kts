plugins {
    kotlin("multiplatform") version "1.6.0-M1"
}

repositories {
    mavenCentral()
}

kotlin {
    jvm()

    sourceSets {
        val commonMain by getting
        val jvmMain by getting
    }
}