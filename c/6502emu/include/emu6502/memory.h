#ifndef MEMORY_H
#define MEMORY_H

#include <stdint.h>

typedef struct _mem memory;

memory create_memory(uint32_t size);
void destroy_memory(memory memory);

#endif // MEMORY_H