#include "windows-portability.h"

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <argp.h>

const char* argp_program_version = "touchmk 1.0.0";
const char* argp_program_bug_address = "<none>";
static char doc[] = "Will create file directory structures and update 'mtime' on files/directories";
static char args_doc[] = "[FILENAME] FILEPATH";
static struct argp_option options[] = {  };

typedef struct {
    char* filepath
} arguments;

static error_t parse_opt(int key, char* arg, struct argp_state* state) {
    arguments* args = state->input;

    switch (key) {
        case ARGP_KEY_ARG:
            args->filepath = arg;
        default: return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc, 0, 0, 0 };

int main(int argc, char** argv) {
    arguments args;

    args.filepath = NULL;

    char** argv_copy;
    memcpy(argv_copy, argv, sizeof(char*) * argc);

    argp_parse(&argp, argc, argv, 0, 0, &args);

    if (args.filepath == NULL) {
        printf("Usage: %s FILEPATH", argv_copy[0]);
        return EINVAL;
    }

    const char* filepath = args.filepath;

    char *token, *str, *tofree;

    tofree = str = strdup(filepath);  // We own str's memory now.
    while ((token = strsep(&str, ","))) my_fn(token);
    free(tofree);
}