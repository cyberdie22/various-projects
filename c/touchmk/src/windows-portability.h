#ifndef WINDOWS_PORTABILITY_H
#define WINDOWS_PORTABILITY_H

#if defined(_WIN32)
char *strsep(char **stringp, const char *delim) {
  if (*stringp == NULL) { return NULL; }
  char *token_start = *stringp;
  *stringp = strpbrk(token_start, delim);
  if (*stringp) {
    **stringp = '\0';
    (*stringp)++;
  }
  return token_start;
}
#endif

#endif // WINDOWS_PORTABILITY_H