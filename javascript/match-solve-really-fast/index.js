import fs from 'fs'

import HtmlParser from 'node-html-parser'
const { parse } = HtmlParser

const test = fs.readFileSync('test.html')

const doc = parse(test.toString())
const matches = {}
const matched = {
    'Boolean Expression': 'Evalutes to True or False.',
    'Evalutes to True or False.': 'Boolean Expression',
    'Conditionals': 'Another term for if statements -- statements that only run under certain conditions.',
    'Another term for if statements -- statements that only run under certain conditions.': 'Conditionals',
    'If statements': 'Evaluate a Boolean condition and execute if the condition is true',
    'Evaluate a Boolean condition and execute if the condition is true': 'If statements',
    '4==4': 'Equal to',
    'Equal to': '4==4',
    '3!=4': 'Not Equal to',
    'Not Equal to': '3!=4',
    '10>9': 'Greater Than', // requires special handling
    'Greater Than': '10>9',
    '9<10': 'Less Than',
    'Less Than': '9<10',
    '10>=18': 'Greater than OR Equal to', // requires special handling
    'Greater than OR Equal to': '10>=18',
    '10<=18': 'Less than OR Equal to',
    'Less than OR Equal to': '10<=18',
    'Comparison operator': 'Compares two values and returns either TRUE or FALSE',
    'Compares two values and returns either TRUE or FALSE': 'Comparison operator',
    'keyDown': 'Code for controlling a sprite with a keyboard input',
    'Code for controlling a sprite with a keyboard input': 'keyDown',
    'var=4': 'Assigns (gets) a value',
    'Assigns (gets) a value': 'var=4',
    'If/Else Statement': 'If this condition is true, do this, or else (if false) do something else',
    'If this condition is true, do this, or else (if false) do something else': 'If/Else Statement',
    '{ }': 'Curly Braces represent the start and end of a block of code',
    'Curly Braces represent the start and end of a block of code': '{ }',
    'mouseDown': 'Code for controlling a sprite with a mouse click',
    'Code for controlling a sprite with a mouse click': 'mouseDown',
    'mouseDidMove': 'Code for detecting mouse movement',
    'Code for detecting mouse movement': 'mouseDidMove',
    'console.log': 'Prints text to the console below the workspace',
    'Prints text to the console below the workspace': 'console.log',
    'Counter Pattern': 'Keeps track of increasing or dcreasing numbers or movement in your code',
    'Keeps track of increasing or dcreasing numbers or movement in your code': 'Counter Pattern',
    'watcher': 'Keeps track of the variables in your code to help you debug it',
    'Keeps track of the variables in your code to help you debug it': 'watcher',
    'variable': 'A placeholder for information that can change',
    'A placeholder for information that can change': 'variable',
    'Binary': 'A numbering system that uses only zeros and ones',
    'A numbering system that uses only zeros and ones': 'Binary',
}

var cards = []
var nodes = []
var nodeCount = 0
const root = doc.childNodes[0]
root.childNodes.forEach(node => {
    node.childNodes.forEach(node2 => {
        if (node2.rawAttrs) {
            if (node2.rawAttrs.includes('aria-label')) {
                nodes.push(node2)
            }
        }
    })
})

// var used = []
nodes.forEach(node => {
    var nodeText = node.childNodes[0]._rawText.replace('&gt;', '>').replace('&lt;', '<')
    nodes.forEach(node2 => {
        var node2Text = node2.childNodes[0]._rawText.replace('&gt;', '>').replace('&lt;', '<')
        console.log(`Node 1: ${nodeText}, Node 2: ${node2Text}, ${node2.childNodes}`)
        if (nodeText.includes('10>9') && matched['10>9'] === node2Text) {
            cards.push([node, node2])
        }
        if (nodeText.includes('10>=18') && matched['10>=18'] === node2Text) {
            cards.push([node, node2])
        }
        if (matched[nodeText] === node2Text) {
            cards.push([node, node2])
        }
    })
})

console.log(cards)