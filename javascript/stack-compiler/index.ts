import fs from 'fs'
import path from 'path'

const ERROR = (message: string) => {
    console.error(`ERROR: ${message}`)
    process.exit(1)
}

const ASSERT = (expr: boolean, message: string) => {
    if (!expr) ERROR(message)
}

const TODO = (message: string) => {
    ERROR(`TODO: ${message}`)
}

const NO_IMPL = (name: string) => {
    TODO(`Not implemented. (${name})`)
}

enum TokenKind {
    PushInt = 0,
    Add,
    Print,
    __LENGTH__,
}

class Token {
    kind: TokenKind
    value: string

    constructor(kind: TokenKind, value: string) {
        this.kind = kind
        this.value = value
    }
}

const NUMBER = /[0-9]+/

const tokenize = (file: string): Array<Token> => {
    ASSERT(TokenKind.__LENGTH__ === 3, 'The amount of token kinds has changed, please update `tokenize`')
    const split = file.split(' ')
    const tokens: Array<Token> = []
    
    split.forEach((it: string) => {
        if (NUMBER.exec(it)) {
            tokens.push(new Token(TokenKind.PushInt, it))
        }
        
        if (it === 'print') {
            tokens.push(new Token(TokenKind.Print, it))
        }

        if (it === '+') {
            tokens.push(new Token(TokenKind.Add, it))
        }
    })
    return tokens
}

const generate_x86_64_linux_nasm = (tokens: Array<Token>) => {
    
}

const simulate = (tokens: Array<Token>) => {
    const stack = []

    tokens.forEach((it: Token) => {
        ASSERT(TokenKind.__LENGTH__ === 3, 'The amount of token kinds has changed, please update `simulate`')
        switch (it.kind) {
            case TokenKind.PushInt:
                stack.push(parseInt(it.value))
                break
            case TokenKind.Add:
                const a = stack.pop()
                const b = stack.pop()
                stack.push(a + b)
                break
            case TokenKind.Print:
                const num = stack.pop()
                console.log(num)
                break
            default:
                ERROR('Simulation: unreachable')
        }
    })
}

const usage = () => {
    console.error(`
    Usage:
        ${process.argv[1]} <OPTIONS> SUBCOMMAND

        SUBCOMMANDS:
            compile <TARGET> <FILE>
            TARGETS:
                x86_64-linux-nasm

            simulate <FILE>
    `)
}

// function main
(() => {
    let mode: string
    let target: string
    let file: string
    
    const args = process.argv.slice(2)

    mode = args.shift()
    if (mode !== 'compile' && mode !== 'simulate') {
        usage()
        ERROR(`Invalid mode ${mode}`)
    }

    if (mode === 'compile') {
        target = args.shift()
        if (target !== 'x86_64-linux-nasm') {
            usage()
            ERROR(`Invalid target '${target}'`)
        }
        file = args.shift()
    } else if (mode === 'simulate') {
        target = null
        file = args.shift()
    } else {
        usage()
        ERROR(`This should be impossible, unknown mode '${mode}''`)
    }

    console.log(`mode: ${mode}, target: ${target}, file: ${file}`)

    const program = fs.readFileSync(path.join(__dirname, file)).toString()
    const program_tokens = tokenize(program)
    
    if (mode === 'simulate') simulate(program_tokens)
    else if (mode === 'compile') {
        switch (target) {
            case 'x86_64-linux-nasm':
                generate_x86_64_linux_nasm(program_tokens)
                break
            default:
                usage()
                ERROR(`This should be impossible, unknown target '${target}'}`)
        }
    } else {
        usage()
        ERROR(`This should be impsossible, unknown mode '${mode}'`)
    }
})();