#!/usr/bin/env node
import esbuild from 'esbuild'

esbuild.build({
    entryPoints: ['index.ts'],
    bundle: true,
    outdir: 'build',
    format: 'cjs',
    minify: true,
    treeShaking: true,
    platform: 'node',
    sourcemap: true,
    target: [
        'es2020',
        'node12'
    ],
    watch: true,
    allowOverwrite: true,
    color: true,
    incremental: true,
    outExtension: { '.js': '.cjs' }
}).then((result) => {
    console.log('not stopping')
})