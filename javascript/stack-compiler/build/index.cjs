var f=Object.create;var u=Object.defineProperty;var d=Object.getOwnPropertyDescriptor;var _=Object.getOwnPropertyNames;var k=Object.getPrototypeOf,T=Object.prototype.hasOwnProperty;var b=s=>u(s,"__esModule",{value:!0});var E=(s,e,n)=>{if(e&&typeof e=="object"||typeof e=="function")for(let t of _(e))!T.call(s,t)&&t!=="default"&&u(s,t,{get:()=>e[t],enumerable:!(n=d(e,t))||n.enumerable});return s},m=s=>E(b(u(s!=null?f(k(s)):{},"default",s&&s.__esModule&&"default"in s?{get:()=>s.default,enumerable:!0}:{value:s,enumerable:!0})),s);var p=m(require("fs")),h=m(require("path")),a=s=>{console.error(`ERROR: ${s}`),process.exit(1)},g=(s,e)=>{s||a(e)};var i;(function(o){o[o.PushInt=0]="PushInt",o[o.Add=1]="Add",o[o.Print=2]="Print",o[o.__LENGTH__=3]="__LENGTH__"})(i||(i={}));var c=class{constructor(e,n){this.kind=e,this.value=n}},A=/[0-9]+/,$=s=>{g(!0,"The amount of token kinds has changed, please update `tokenize`");let e=s.split(" "),n=[];return e.forEach(t=>{A.exec(t)&&n.push(new c(0,t)),t==="print"&&n.push(new c(2,t)),t==="+"&&n.push(new c(1,t))}),n},O=s=>{},x=s=>{let e=[];s.forEach(n=>{switch(g(!0,"The amount of token kinds has changed, please update `simulate`"),n.kind){case 0:e.push(parseInt(n.value));break;case 1:let t=e.pop(),o=e.pop();e.push(t+o);break;case 2:let l=e.pop();console.log(l);break;default:a("Simulation: unreachable")}})},r=()=>{console.error(`
    Usage:
        ${process.argv[1]} <OPTIONS> SUBCOMMAND

        SUBCOMMANDS:
            compile <TARGET> <FILE>
            TARGETS:
                x86_64-linux-nasm

            simulate <FILE>
    `)};(()=>{let s,e,n,t=process.argv.slice(2);s=t.shift(),s!=="compile"&&s!=="simulate"&&(r(),a(`Invalid mode ${s}`)),s==="compile"?(e=t.shift(),e!=="x86_64-linux-nasm"&&(r(),a(`Invalid target '${e}'`)),n=t.shift()):s==="simulate"?(e=null,n=t.shift()):(r(),a(`This should be impossible, unknown mode '${s}''`)),console.log(`mode: ${s}, target: ${e}, file: ${n}`);let o=p.default.readFileSync(h.default.join(__dirname,n)).toString(),l=$(o);if(s==="simulate")x(l);else if(s==="compile")switch(e){case"x86_64-linux-nasm":O(l);break;default:r(),a(`This should be impossible, unknown target '${e}'}`)}else r(),a(`This should be impsossible, unknown mode '${s}'`)})();
//# sourceMappingURL=index.cjs.map
