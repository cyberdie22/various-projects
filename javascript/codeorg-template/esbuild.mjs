import esbuild from 'esbuild';

esbuild.build({
    entryPoints: ['src/test.js'],
    bundle: true,
    minify: true,
    outfile: 'out/out.js',
    treeShaking: true,
})