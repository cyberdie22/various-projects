export const avaliableKeys = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', 
'1', '2', '3', '4', '5', '6', '7', '8', '9', '`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '=', '{', '[', ']', '}', 
'\\', '|', ':', ';', '"', "'", '<', ',', '>', '.', '?', '/', 'tab', 'esc', 'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10', 'f11', 
'f12', 'insert', 'home', 'delete', 'end', 'up', 'down', 'left', 'right', 'space']
export const avaliableButtons = ['left', 'right']

export interface Signal {}

export interface DrawEvent extends Signal {}

export class StartDrawEvent implements DrawEvent {}
export class EndDrawEvent implements DrawEvent {}

export interface MouseEvent extends Signal {
    button?: string;
}
export interface KeyEvent extends Signal {
    key: string
}
export interface SpriteEvent extends Signal {
    sprite: Sprite
}

export class MouseDownEvent implements MouseEvent {
    button: string

    constructor(key: string) {
        this.button = key
    }
}
export class MouseHoldEvent implements MouseEvent {
    button: string

    constructor(key: string) {
        this.button = key
    }
}
export class MouseReleaseEvent implements MouseEvent {
    button: string

    constructor(key: string) {
        this.button = key
    }
}
export class MouseMoveEvent implements MouseEvent {
}

export class KeyDownEvent implements KeyEvent {
    key: string

    constructor(key: string) {
        this.key = key
    }
}
export class KeyHoldEvent implements KeyEvent {
    key: string

    constructor(key: string) {
        this.key = key
    }
}
export class KeyReleaseEvent implements KeyEvent {
    key: string

    constructor(key: string) {
        this.key = key
    }
}

export class SpriteStartHoverEvent implements SpriteEvent {
    spritearr: Array<Sprite>

    get sprite(): Sprite { 
        return this.spritearr[0]
    }

    constructor(sprite: Array<Sprite>) {
        if (sprite.length !== 1) throw new Error(`Invalid sprite array length: ${sprite.length}`)
        this.spritearr = sprite
    }
}
export class SpriteHoveredEvent implements SpriteEvent {
    spritearr: Array<Sprite>

    get sprite(): Sprite { 
        return this.spritearr[0]
    }

    constructor(sprite: Array<Sprite>) {
        if (sprite.length !== 1) throw new Error(`Invalid sprite array length: ${sprite.length}`)
        this.spritearr = sprite
    }
}
export class SpriteEndHoverEvent implements SpriteEvent {
    spritearr: Array<Sprite>

    get sprite(): Sprite { 
        return this.spritearr[0]
    }

    constructor(sprite: Array<Sprite>) {
        if (sprite.length !== 1) throw new Error(`Invalid sprite array length: ${sprite.length}`)
        this.spritearr = sprite
    }
}
export class SpriteMouseDownEvent implements SpriteEvent, MouseEvent {
    spritearr: Array<Sprite>
    button: string

    get sprite(): Sprite { return this.spritearr[0] }

    constructor(sprite: Array<Sprite>, button: string) {
        if (sprite.length !== 1) throw new Error(`Invalid sprite array length: ${sprite.length}`)
        this.spritearr = sprite
        this.button = button
    }
}
export class SpriteMouseReleaseEvent implements SpriteEvent, MouseEvent {
    spritearr: Array<Sprite>
    button: string

    get sprite(): Sprite { return this.spritearr[0] }

    constructor(sprite: Array<Sprite>, button: string) {
        if (sprite.length !== 1) throw new Error(`Invalid sprite array length: ${sprite.length}`)
        this.spritearr = sprite
        this.button = button
    }
}

export enum EventKind {
    StartDraw = 'StartDrawEvent',
    EndDraw = 'EndDrawEvent',
    MouseDown = 'MouseDownEvent',
    MouseHold = 'MouseHoldEvent',
    MouseRelease = 'MouseReleaseEvent',
    MouseMove = 'MouseMoveEvent',
    KeyDown = 'KeyDownEvent',
    KeyHold = 'KeyHoldEvent',
    KeyRelease = 'KeyReleaseEvent',
    SpriteStartHover = 'SpriteStartHoverEvent',
    SpriteHovered = 'SpriteHoveredEvent',
    SpriteEndHover = 'SpriteEndHoverEvent',
    SpriteMouseDown = 'SpriteMouseDownEvent',
    SpriteMouseReleaseEvent = 'SpriteMouseReleaseEventEvent',
}

export function checkKey(key: string): void {
    if (keyWentDown(key)) EventHandler.dispatch(EventKind.KeyDown, new KeyDownEvent(key))
    if (keyDown(key)) EventHandler.dispatch(EventKind.KeyHold, new KeyHoldEvent(key))
    if (keyWentUp(key)) EventHandler.dispatch(EventKind.KeyRelease, new KeyReleaseEvent(key))
}

export function checkButton(button: string): void {
    if (mouseWentDown(button)) EventHandler.dispatch(EventKind.MouseDown, new MouseDownEvent(button))
    if (mouseDown(button)) EventHandler.dispatch(EventKind.MouseHold, new MouseHoldEvent(button))
    if (mouseWentUp(button)) EventHandler.dispatch(EventKind.MouseRelease, new MouseReleaseEvent(button))
}

class EventHandlerClass {
    private handlers = {}

    on<T>(eventKind: EventKind, handler: (event: T) => void) {
        let handlerArray = this.handlers[eventKind]
        if (!handlerArray) {
            handlerArray = []
        }
        handlerArray.push(handler)
        this.handlers[eventKind] = handlerArray
    }

    dispatch(eventKind: EventKind, event: Signal) {
        let eventHandlers = this.handlers[eventKind]
        if (eventHandlers) {
            eventHandlers.forEach(handler => handler(event))
        }
    }
}

export const EventHandler = new EventHandlerClass()