function getRandomPos() {
return randomNumber(50, 350);
}
var player = createSprite(200, 100);
player.setAnimation("fly_bot");
player.scale = 0.8;
var coin = createSprite(getRandomPos(), getRandomPos());
coin.setAnimation("coin");
var blockDown = createSprite(getRandomPos(), getRandomPos());
blockDown.setAnimation("rock");
blockDown.velocityY += 5;
var blockRight = createSprite(getRandomPos(), getRandomPos());
blockRight.setAnimation("rock");
blockRight.velocityX += 5;
var gameOver = false;
var gravity = 10;
var upward = -5
if (antiGravity) {
    gravity = -gravity;
    upward = -upward
}
function draw() {
    if (gameOver) {
        background("black");
        textSize(50);
        fill("green");
        text("Game Over!", 50, 200);
        return;
    }
    background("lightblue");
    player.y += gravity;
    if (keyDown("up"))
        player.y += -gravity + upward;
    if (keyDown("left"))
        player.x -= 5;
    if (keyDown("right"))
        player.x += 5;
    if (player.isTouching(coin)) {
        coin.x = getRandomPos();
        coin.y = getRandomPos();
    }
    if (blockRight.x > 400) {
        blockRight.x = -100;
        blockRight.y = getRandomPos();
    }
    if (blockDown.y > 400) {
        blockDown.y = -100;
        blockDown.x = getRandomPos();
    }
    blockRight.displace(player);
    blockDown.displace(player);
    drawSprites();
    if (player.x < -50 || player.x > 450 || player.y < -50 || player.y > 450) {
        gameOver = true;
    }
}
draw()