import { EventHandler, EventKind, avaliableButtons } from "./events"

class UtilsClass {
    private _currentButton: string | undefined = undefined
    private _lastButton: string | undefined = undefined
    get currentButton() { return this._currentButton }
    get lastButton() { return this._lastButton }

    constructor() {
        EventHandler.on(EventKind.MouseDown, (button: string) => {
            this._currentButton = button
        })

        EventHandler.on(EventKind.MouseRelease, (_: string) => {
            this._lastButton = this._currentButton
            this._currentButton = undefined
        })
    }
}

export default new UtilsClass();