import { KeyEvent, StartDrawEvent, EndDrawEvent, MouseMoveEvent, SpriteStartHoverEvent, SpriteEndHoverEvent, 
    SpriteMouseDownEvent, SpriteMouseReleaseEvent, EventHandler, EventKind, SpriteHoveredEvent, avaliableKeys as keys, avaliableButtons as buttons, checkKey, checkButton } from './events'
import Utils from './utils'

function makeSprite(x: number, y: number, width?: number, height?: number): Sprite {
    const sprite = createSprite(x, y, width, height)
    sprite.onMouseOver = (sprite) => { EventHandler.dispatch(EventKind.SpriteStartHover, new SpriteStartHoverEvent([sprite])) }
    sprite.onMouseOut = (sprite) => { EventHandler.dispatch(EventKind.SpriteEndHover, new SpriteEndHoverEvent([sprite])) }
    sprite.onMousePressed = (sprite) => {
        const button = Utils.currentButton
        EventHandler.dispatch(EventKind.SpriteMouseDown, new SpriteMouseDownEvent([sprite], button))
    }
    sprite.onMouseReleased = (sprite) => { 
        const button = Utils.lastButton
        EventHandler.dispatch(EventKind.SpriteStartHover, new SpriteMouseReleaseEvent([sprite], button)) 
    }
    return sprite
}
EventHandler.on(EventKind.SpriteHovered, (event: SpriteHoveredEvent) => {
    event.sprite.setAnimation("animation2")
    setInterval(() => event.sprite.setAnimation("animation1"), 1000)
    console.log(event)
})


const sprite = createSprite(100, 100)
sprite.setAnimation("animation1")

function draw() {
    EventHandler.dispatch(EventKind.StartDraw, new StartDrawEvent())
    keys.forEach(key => checkKey(key))
    buttons.forEach(button => checkButton(button))
    if (mouseDidMove()) EventHandler.dispatch(EventKind.MouseMove, new MouseMoveEvent())
    World.allSprites.forEach(sprite => { 
        if (mouseIsOver(sprite)) {
            const event = new SpriteHoveredEvent([sprite])
            EventHandler.dispatch(EventKind.SpriteHovered, event)
            sprite = event.sprite 
        }
    })
    EventHandler.dispatch(EventKind.EndDraw, new EndDrawEvent())
    drawSprites()
}
draw()
