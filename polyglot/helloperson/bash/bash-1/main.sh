#!/usr/bin/env bash
if [[ $# -ne 1 ]]; then
    echo "Usage:"
    echo ""
    echo "$0 NAME"
    exit 1
fi

echo "Hello $1!"