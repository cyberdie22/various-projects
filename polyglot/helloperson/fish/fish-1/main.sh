#!/usr/bin/env fish
if test (count $argv) -ne 1
    echo "Usage:"
    echo ""
    echo "./main.sh NAME"
    exit 1
end

echo "Hello $argv[1]!"