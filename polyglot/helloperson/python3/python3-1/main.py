#!/usr/bin/env python3
import sys

if len(sys.argv) <= 1:
    print("Usage:")
    print("")
    print("%s NAME" % (sys.argv[0]))
    sys.exit(1)

print("Hello %s!" % (sys.argv[1]))