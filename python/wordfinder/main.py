#!/usr/bin/env python3

def load_words():
    with open('words.txt') as word_file:
        valid_words = set(word_file.read().split())

    return valid_words

def str_to_bool(str: str) -> bool:
    return str == "true"

words = load_words()

case_sensitive = str_to_bool(input("is case sensitive (true/false): "))
commands = input("input (fl L = first letter, ll L = last letter, len I = length, cont S): ")
split_commands = commands.split(", ")

firstLetter = None
lastLetter = None
length = None
contains = None
for command in split_commands:
    split = command.split(" ")
    if split[0] == "fl":
        firstLetter = split[1]
    elif split[0] == "ll":
        lastLetter = split[1]
    elif split[0] == "len":
        length = int(split[1])
    elif split[0] == "cont":
        contains = split[1]
    else:
        print("unknown command: " + command)

possibilities = []
for word in words:
    if (not case_sensitive):
        word = word.lower()

    has_firstletter = (firstLetter == None or word.startswith(firstLetter))
    has_lastletter  = (lastLetter  == None or word.endswith(lastLetter))
    has_length      = (length      == None or len(word) == length)
    has_contains    = (contains    == None or contains in word)
    if has_firstletter and has_lastletter and has_length and has_contains:
        possibilities.append(word)

print(possibilities)